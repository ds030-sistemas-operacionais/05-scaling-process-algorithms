# ACTIVITY 05 - Scaling process algorithms

## First Come, Fist Served - FCFS | Shortest Job First - SJF | Round-Robin

----
### *All of the algorithms must run in the ufpr.farma-alg environment*
----
Implementation of 3 scaling process's algorithms:
- First Come, First Served - FCFS - As sugested by the name, the first job to enter the processor will be the first to be processed 
- Shortest Job First - SJF - The shortest jobs will be processed first
- Round-Robin - The processor can be alocated for a job for the maximum amount of time defined by the quantum factor (q) - That's defined by the user during the runtime
