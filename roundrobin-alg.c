#include<stdio.h>
#include<stdlib.h>

typedef struct _PROCESS {
	int p;
	int t;
} PROCESS;

PROCESS *alocaMem(int n);
void writeProcess(PROCESS *pr, int q);
int processa(PROCESS *pr, int q);
int checkProcess(PROCESS *pr, int *c);

int main(int argv, char *argc[]) {
	int q, n, f = 0, i, ttotal = 0, espera = 0, final = 0, count = 0, last = 0;
	float result;
	PROCESS *proc;
	scanf("%d %d", &n, &q);
	proc = alocaMem(n);
	writeProcess(proc, n);
	while (f < n) {
		for (i = 0; i < n; i++) {
			if (proc[i].t > 0) {
				if ((n - f) == 1) {
					if(last == 0) {
						final = espera;
						espera += espera + count;
						last = 1;
					}
					final += count;
					printf("%dms: Processo %d alocado\n", final, proc[i].p);
					count = processa(&proc[i], q);
					printf("--- Processo %d %s\n", proc[i].p, (checkProcess(&proc[i], &f) == 1 ? "concluido" : "saiu por preempcao"));
					ttotal += count;
				} else {
					espera += count;
					printf("%dms: Processo %d alocado\n", espera, proc[i].p);
					count = processa(&proc[i], q);
					printf("--- Processo %d %s\n", proc[i].p, (checkProcess(&proc[i], &f) == 1 ? "concluido" : "saiu por preempcao"));
					ttotal += count;
				}
			}
		}
	}
	result = (double)espera/n;
	result = (float)espera / n;
	printf("\n>>> >>> Todos os processos concluidos\nTempo total: %dms\nTempo medio de espera: %.2fms", ttotal, ((int)(result * 100)/100.0));
	return 0;
}

PROCESS *alocaMem(int n) {
	PROCESS *pr;
	pr = (PROCESS *)malloc(n * sizeof(PROCESS));
	if (!pr) {
		printf("Problema na alocacao\n");
		exit(0);
	}
	return pr;
}

void writeProcess(PROCESS *pr, int q) {
	int i;
	for (i = 0; i < q; i++) {
		scanf("%d %d", &pr[i].p, &pr[i].t);
	}
}

int processa(PROCESS *pr, int q) {
	int i, count = 0;
	for (i = 0; i < q && pr->t > 0; i++,pr->t--) {
		count++;
	}
	return count;
}

int checkProcess(PROCESS *pr, int *c) {
	if (pr->t == 0) {
		(*c)++;
		return 1;
	}
	else return 0;
}
